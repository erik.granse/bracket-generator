package net.loonmagnet;

import java.util.ArrayList;
import java.util.List;

public class Round {


    public Round(List<Matchup> matchups, String region, int round) {

        System.out.println(region + " region, round " + round);

        if (matchups.size() == 1) {
            matchups.get(0).playGame();
        } else {
            List<Matchup> nextMatchups = new ArrayList<>();
            for (int i = 0; i < matchups.size(); i += 2) {
                nextMatchups.add(new Matchup(matchups.get(i).playGame(), matchups.get(i + 1).playGame()));
            }
            new Round(nextMatchups, region, round + 1);
        }
    }
}
