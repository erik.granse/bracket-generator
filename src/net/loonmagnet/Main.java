package net.loonmagnet;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> regions = new ArrayList<>();
        regions.add("East");
        regions.add("West");
        regions.add("Midwest");
        regions.add("South");

        for (String region : regions) {
            List<Matchup> matchups = new ArrayList<>();
            matchups.add(new Matchup(1, 16));
            matchups.add(new Matchup(8, 9));
            matchups.add(new Matchup(5, 12));
            matchups.add(new Matchup(4, 13));
            matchups.add(new Matchup(6, 11));
            matchups.add(new Matchup(3, 14));
            matchups.add(new Matchup(7, 10));
            matchups.add(new Matchup(2, 15));
            new Round(matchups, region, 1);

            System.out.println();
        }
    }
}
