package net.loonmagnet;

import java.util.concurrent.ThreadLocalRandom;

public class Matchup {

    final int team1SeedValue;
    final int team2SeedValue;
    final int totalSeedValue;
    final int bestSeed;
    final int worstSeed;
    final int randomNum;
    final int winner;

    public Matchup(int team1SeedValue, int team2SeedValue) {
        this.team1SeedValue = team1SeedValue;
        this.team2SeedValue = team2SeedValue;
        this.totalSeedValue = team1SeedValue + team2SeedValue;
        this.bestSeed = Math.min(team1SeedValue, team2SeedValue);
        this.worstSeed = Math.max(team1SeedValue, team2SeedValue);
        this.randomNum = ThreadLocalRandom.current().nextInt(1, totalSeedValue);
        if (randomNum < worstSeed) {
            this.winner = bestSeed;
        } else {
            this.winner = worstSeed;
        }
    }

    @Override
    public String toString() {
        return "\tMatchup{" +
                "team1SeedValue=" + team1SeedValue +
                ", team2SeedValue=" + team2SeedValue +
                ", totalSeedValue=" + totalSeedValue +
                ", bestSeed=" + bestSeed +
                ", worstSeed=" + worstSeed +
                ", randomNum=" + randomNum +
                ", winner=" + winner +
                '}';
    }

    public int playGame() {
        System.out.println(this.toString());
        return winner;
    }
}
